terraform {
  backend "s3" {
        bucket = "s3statebackend280902"
        dynamodb_table = "state-lock2"
        key = "terraform.tfstate"
        region = "us-east-1"
        //profile = "my-profile"
        
    }
     required_providers {
    aws = {
      source  = "hashicorp/aws"
       version = "5.41.0"
    }
  }   
} 