//child module:
#ec2 creation:

resource "aws_instance" "server" {
    ami = "ami-0d7a109bf30624c99" 

    instance_type = "t2.micro"
    subnet_id = var.sn //from another module
    security_groups = [var.sg] //from another module

    tags = {
        Name = "myserver"
    }
  
}